import java.io.*;
import java.net.Socket;
import java.util.HashSet;
import java.util.Set;

import static java.lang.Thread.sleep;

public class Catclient {
    public static void main(String[] args){
        if(args.length != 3){
            System.err.println("Invalid argument number, 3 expected, e.g. file port host");
            System.exit(1);
        }
        String hostName = args[2];
        int portNumber = Integer.parseInt(args[1]);
        String filePath = args[0];
        String line;
        try {
            BufferedReader br = new BufferedReader(new FileReader(filePath));
            Set<String> contents = new HashSet<String>();
            while((line=br.readLine()) != null){
                contents.add(line.toUpperCase());
            }
            br.close();

            Socket echoSocket = new Socket(hostName, portNumber);
            PrintWriter out = new PrintWriter(echoSocket.getOutputStream(), true);
            BufferedReader in = new BufferedReader(new InputStreamReader(echoSocket.getInputStream()));

            long currentTime = System.currentTimeMillis();
            long endTime = currentTime + 30000;
            while(System.currentTimeMillis() < endTime){
                out.println("LINE");
                line = in.readLine();
                if(contents.contains(line)){
                    System.out.println("OK");
                }else{
                    System.out.println("MISSING");
                }
                try {
                    sleep(3000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
