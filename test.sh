echo "Test start, building datacenter"
cd datacenter
docker build -t datacenter .
docker run -dt --name datacontainer datacenter

echo "Datacenter built done, now build catserver"
cd ../catserver
docker build -t catserver .
docker run -idt --name serverinstance --volumes-from datacontainer catserver /bin/bash -c "cd /src; javac Catserver.java; java Catserver ../data/string.txt 5432"
host=$(docker inspect --format='{{json .NetworkSettings.Networks.bridge.IPAddress}}' serverinstance)

echo "Server instance now running, build client"
cd ../catclient
docker build -t catclient .
docker run -idt --name clientinstance --volumes-from datacontainer catclient /bin/bash -c "cd /src; javac Catclient.java; java Catclient ../data/string.txt 5432 $host"

echo "Checking output"
docker logs -f clientinstance

echo "Test finishes."
