import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;

public class Catserver{
    public static void main(String[] args){
        if(args.length != 2){
            System.err.println("Invalid argument number, 2 expected, e.g. file port");
            System.exit(1);
        }
        String filePath = args[0];
        int port = Integer.parseInt(args[1]);
        String line;
        try {
            BufferedReader br = new BufferedReader(new FileReader(filePath));
            ArrayList<String> contents = new ArrayList<String>();
            while((line=br.readLine()) != null){
                contents.add(line.toUpperCase());
            }
            br.close();

            ServerSocket myServer = new ServerSocket(port);
            System.out.println("Listening to port " + args[1]);

            while(true) {
                Socket socket = myServer.accept();
                System.out.println("New connection!");
                BufferedReader is = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                PrintStream os = new PrintStream(socket.getOutputStream());

                int index = 0;
                while ((line = is.readLine()) != null) {
                    if (line.equals("LINE")) {
                        os.println(contents.get(index));
                        index = (index + 1) % contents.size();
                    } else {
                        os.println("The server only respond to LINE");
                    }
                }
                System.out.println("Disconnected");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
